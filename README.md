bs-tea-feather-icons
====================

Synopsis
--------
bs-tea-feather-icons contains bindings to the feather icons to be used
with [Bucklescript-TEA](https://github.com/OvermindDL1/bucklescript-tea).


Installation
------------

```
yarn add bs-tea-feather-icons
```

And don't forget to add it to your bsconfig.json:

```
  "bs-dependencies": [
    "bs-tea-feather-icons",
    ...
  ],

```


Usage
-----

```
let view model =
    TeaFeatherIcons.airplay []
```


Bug tracker
-----------
[Open a new issue](https://gitlab.com/schutm/bs-tea-feather-icons/issues) for bugs
or feature requests. Please search for existing issues first.

Bugs or feature request will be fixed in the following order, if time
permits:

1. It has a pull-request with a working and tested fix.
2. It is easy to fix and has benefit to myself or a broader audience.
3. It puzzles me and triggers my curiosity to find a way to fix.


Acknowledgements
----------------
Of course this library would be impossible without the excellent
[Feather icons](https://www.feathericons.com)


Contributing
------------
Anyone and everyone is welcome to [contribute](CONTRIBUTING.md).


License
-------
This software is licensed under the [ISC License](LICENSE).
