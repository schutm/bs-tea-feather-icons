Release 0.0.11
--------------
Have it finally working under the name TeaFeatherIcons. Phewww...


Release 0.0.10
--------------
Drop namespace alltogether to remove bs-prefix and still have everything working.


Release 0.0.9
-------------
Drop the bs- prefix and correct the documentation accordingly.


Release 0.0.8
-------------
Have a correct TEA structure emitted.


Release 0.0.7
-------------
I hope these are the last bugs to be fixed and compile the library.


Release 0.0.6
-------------
Reorderd generate ocaml code (so it will compile).


Release 0.0.5
-------------
Exclude the Feather icon namespace, as it is already included by TEA.


Release 0.0.4
-------------
Really fix the bug, which prevented bucklescript to compile this library.


Release 0.0.3
-------------
Fixed some bugs, which prevented bucklescript to compile this library.


Release 0.0.2
-------------
Fixed some bugs, which prevented bucklescript to compile this library.


Release 0.0.1
-------------
Initial release to automatically import feather icons and create bucklescript bindings.
