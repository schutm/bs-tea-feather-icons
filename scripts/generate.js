const camelCase = require('camelcase');
const snakeCase = require('js-snakecase');
const feather = require('feather-icons');
const xmlParser = require('xml-js');
const fs = require('fs');

function createIconFunction(icon) {
    function dekeyword(word) {
        const keywords = ['class', 'type'];

        return (keywords.includes(word) ? word + "'" : word);
    }

    function includeAttribute(attribute) {
        const excludes = ['xmlns'];

        return !excludes.includes(attribute);
    }

    function mapElement(element) {
        let name = element.name;
        let attributes = Object.keys(element.attributes || {}).filter(includeAttribute).map(
            (key) => `A.${dekeyword(camelCase(key))} "${element.attributes[key]}"`
        )

        return `E.${name} [ ${ attributes.join("; ")} ] [ ]`
    }

    function mapElements(elements) {
        elements = elements || [];

        return elements.map(mapElement);
    }

    let functionName = dekeyword(icon.functionName);
    let defaultAttributes = Object.keys(icon.attrs).filter(includeAttribute).map(
        (key) => `A.${dekeyword(camelCase(key))} "${icon.attrs[key]}"`
    );
    let vdom = mapElements(icon.content.elements);

    return `
let ${functionName} properties =
  let module E = Tea.Svg in
  let module A = Tea.Svg.Attributes in
  let default_attributes =
    [ ${defaultAttributes.join("\n    ; ")}
    ] in
    E.svg (default_attributes @ properties)
      [ ${vdom.join("\n      ; ")}
      ]`
}

const parsedIcons = Object.values(feather.icons).map((icon) => ({
    'functionName': snakeCase(icon.name),
    'tagName': camelCase(icon.name, { pascalCase: true }),
    'tags': icon.tags,
    'attrs': icon.attrs,
    'content': xmlParser.xml2js(icon.contents)
}));

const functions = parsedIcons.map(createIconFunction);

fs.mkdir("./src", {}, () => {
    fs.writeFile("./src/teaFeatherIcons.ml", functions.join("\n"), (err) => {
        if (err) throw err;
    });
});
